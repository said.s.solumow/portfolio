package com.example.javapruefung.service;

import com.example.javapruefung.dto.ReturnStudentDTO;
import com.example.javapruefung.dto.StudentDTO;
import com.example.javapruefung.entity.Lesson;
import com.example.javapruefung.entity.Student;
import com.example.javapruefung.repository.LessonCrudRepository;
import com.example.javapruefung.repository.StudentCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import java.util.Optional;

@Service
public class StudentService {

    @Autowired
    StudentCrudRepository studentCrudRepository;
    @Autowired
    LessonCrudRepository lessonCrudRepository;

    @PostMapping
    public ResponseEntity<?> createStudent(StudentDTO studentDTO) {
        Optional<Lesson> lessonOptional = lessonCrudRepository.findById(studentDTO.getLessonId());

        if (lessonOptional.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        Lesson lesson = lessonOptional.get();
        Student student = new Student(studentDTO.getName(), studentDTO.getAge(), studentDTO.getSocialSecurityNumber(), lesson);
        studentCrudRepository.save(student);

        ReturnStudentDTO returnStudentDTO = new ReturnStudentDTO(student.getStudentId(), student.getName(), student.getAge(), student.getSocialSecurityNumber(), student.getLesson().getLessonId());
        return ResponseEntity.status(HttpStatus.CREATED).body(returnStudentDTO);
    }

    @PutMapping
    public ResponseEntity<?> updateStudentLesson(int studentId, StudentDTO studentDTO) {
        Optional<Lesson> lessonOptional = lessonCrudRepository.findById(studentDTO.getLessonId());
        if (lessonOptional.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        Lesson lesson = lessonOptional.get();

        Optional<Student> studentOptional = studentCrudRepository.findById(studentId);
        if (studentOptional.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        Student student = studentOptional.get();

        student.setLesson(lesson);
        studentCrudRepository.save(student);
        ReturnStudentDTO returnStudentDTO = new ReturnStudentDTO(student.getStudentId(), student.getName(), student.getAge(), student.getSocialSecurityNumber(), student.getLesson().getLessonId());

        return ResponseEntity.status(HttpStatus.OK).body(returnStudentDTO);
    }
}
