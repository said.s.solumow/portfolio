package com.example.javapruefung.service;

import com.example.javapruefung.dto.LessonDTO;
import com.example.javapruefung.dto.ReturnLessonDTO;
import com.example.javapruefung.entity.Lesson;
import com.example.javapruefung.entity.Teacher;
import com.example.javapruefung.repository.LessonCrudRepository;
import com.example.javapruefung.repository.StudentCrudRepository;
import com.example.javapruefung.repository.TeacherCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.*;

@Service
public class LessonService {

    @Autowired
    LessonCrudRepository lessonCrudRepository;
    @Autowired
    TeacherCrudRepository teacherCrudRepository;
    @Autowired
    StudentCrudRepository studentCrudRepository;

    @PostMapping
    public ResponseEntity<?> createLesson(LessonDTO lessonDTO) {
        Optional<Teacher> teacherOptional = teacherCrudRepository.findById(lessonDTO.getTeacherId());

        if (teacherOptional.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        Teacher teacher = teacherOptional.get();

        if (teacher.getSubject() != lessonDTO.getSubject()) {
            Map<String, Object> responseMap = new HashMap<>();
            responseMap.put("Message: ", "Der Trainer unterrichtet " + teacher.getSubject() + "!");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseMap);
        }

        Lesson lesson = new Lesson(lessonDTO.getStartDate(), lessonDTO.getDuration(), lessonDTO.getSubject(), teacher);
        lessonCrudRepository.save(lesson);

        ReturnLessonDTO returnLessonDTO = new ReturnLessonDTO(lesson.getLessonId(), lesson.getStartDate(), lesson.getDuration(), lesson.getSubject(), lesson.getTeacher().getTeacherId());

        return ResponseEntity.status(HttpStatus.CREATED).body(returnLessonDTO);
    }

    @GetMapping
    public ResponseEntity<?> getAllLessons() {
        Iterable<Lesson> lessonIterable = lessonCrudRepository.findAll();
        List<ReturnLessonDTO> lessonDTOList = new ArrayList<>();

        for (Lesson lesson : lessonIterable) {
            ReturnLessonDTO returnLessonDTO = new ReturnLessonDTO(lesson.getLessonId(), lesson.getStartDate(), lesson.getDuration(), lesson.getSubject(), lesson.getTeacher().getTeacherId());
            lessonDTOList.add(returnLessonDTO);
        }

        return ResponseEntity.status(HttpStatus.OK).body(lessonDTOList);
    }

    @DeleteMapping
    public ResponseEntity<?> deleteLesson(int lessonId) {
        if (!lessonCrudRepository.existsById(lessonId)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        lessonCrudRepository.deleteById(lessonId);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
