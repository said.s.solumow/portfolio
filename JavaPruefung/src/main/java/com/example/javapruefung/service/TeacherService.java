package com.example.javapruefung.service;

import com.example.javapruefung.dto.ReturnTeacherDTO;
import com.example.javapruefung.dto.TeacherDTO;
import com.example.javapruefung.entity.Teacher;
import com.example.javapruefung.repository.TeacherCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import java.util.*;

@Service
public class TeacherService {

    @Autowired
    TeacherCrudRepository teacherCrudRepository;

    @PostMapping
    public ResponseEntity<?> createTeacher(TeacherDTO teacherDTO) {
        Teacher teacher = new Teacher(teacherDTO.getName(), teacherDTO.getAge(), teacherDTO.getSubject());
        teacher.setSubject(teacherDTO.getSubject());

        teacherCrudRepository.save(teacher);

        ReturnTeacherDTO returnTeacherDTO = new ReturnTeacherDTO(teacher.getTeacherId(), teacher.getName(), teacher.getAge(), teacher.getSubject());
        return ResponseEntity.status(HttpStatus.CREATED).body(returnTeacherDTO);
    }

    @GetMapping
    public ResponseEntity<?> getAllTrainers() {
        Iterable<Teacher> teacherIterable = teacherCrudRepository.findAll();
        List<ReturnTeacherDTO> teacherDTOList = new ArrayList<>();

        for (Teacher teacher : teacherIterable) {
            ReturnTeacherDTO returnTeacherDTO = new ReturnTeacherDTO(teacher.getTeacherId(), teacher.getName(), teacher.getAge(), teacher.getSubject());
            teacherDTOList.add(returnTeacherDTO);
        }
        return ResponseEntity.status(HttpStatus.OK).body(teacherDTOList);
    }

    @GetMapping
    public ResponseEntity<?> getOneTeacher(int teacherId) {
        Optional<Teacher> teacherOptional = teacherCrudRepository.findById(teacherId);

        if (teacherOptional.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        Teacher teacher = teacherOptional.get();
        ReturnTeacherDTO returnTeacherDTO = new ReturnTeacherDTO(teacher.getTeacherId(), teacher.getName(), teacher.getAge(), teacher.getSubject());

        return ResponseEntity.status(HttpStatus.OK).body(returnTeacherDTO);
    }

    @PutMapping
    public ResponseEntity<?> updateTeacherDetails(int teacherId, TeacherDTO teacherDTO) {
        Optional<Teacher> teacherOptional = teacherCrudRepository.findById(teacherId);

        if (teacherOptional.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        Teacher teacher = teacherOptional.get();
        teacher.setName(teacherDTO.getName());
        teacher.setAge(teacherDTO.getAge());

        if (teacher.getSubject() == null) {
            teacher.setSubject(teacherDTO.getSubject());
        }

        teacherCrudRepository.save(teacher);

        ReturnTeacherDTO returnTeacherDTO = new ReturnTeacherDTO(teacher.getTeacherId(), teacher.getName(), teacher.getAge(), teacher.getSubject());
        return ResponseEntity.status(HttpStatus.OK).body(returnTeacherDTO);
    }
}
