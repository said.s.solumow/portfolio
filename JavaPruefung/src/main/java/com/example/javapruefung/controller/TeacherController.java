package com.example.javapruefung.controller;

import com.example.javapruefung.dto.TeacherDTO;
import com.example.javapruefung.service.TeacherService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/teacher")
public class TeacherController {

    @Autowired
    TeacherService teacherService;

    @PostMapping
    public ResponseEntity<?> createTeacher(@RequestBody TeacherDTO teacherDTO) {
        try {
            return teacherService.createTeacher(teacherDTO);

        } catch (DataIntegrityViolationException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @GetMapping
    public ResponseEntity<?> getAllTrainers() {
        try {
            return teacherService.getAllTrainers();

        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @GetMapping("{teacherId}")
    public ResponseEntity<?> getOneTeacher(@PathVariable int teacherId) {
        try {
            return teacherService.getOneTeacher(teacherId);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @PutMapping("{teacherId}")
    public ResponseEntity<?> updateTeacherDetails(@PathVariable int teacherId, @RequestBody TeacherDTO teacherDTO) {
        try {
            return teacherService.updateTeacherDetails(teacherId, teacherDTO);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }
}
