package com.example.javapruefung.repository;

import com.example.javapruefung.controller.TeacherController;
import com.example.javapruefung.entity.Teacher;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeacherCrudRepository extends CrudRepository<Teacher, Integer> {
}
