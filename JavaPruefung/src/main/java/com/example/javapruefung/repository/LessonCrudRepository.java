package com.example.javapruefung.repository;

import com.example.javapruefung.entity.Lesson;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LessonCrudRepository extends CrudRepository<Lesson, Integer> {
}
