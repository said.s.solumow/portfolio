package com.example.javapruefung.repository;


import com.example.javapruefung.entity.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentCrudRepository extends CrudRepository<Student, Integer> {
}
