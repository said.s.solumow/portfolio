package com.example.javapruefung;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavapruefungApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavapruefungApplication.class, args);
	}

}
