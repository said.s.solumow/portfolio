package com.example.javapruefung.dto;

import com.example.javapruefung.enumerations.Subject;

public class ReturnTeacherDTO {
    private int teacherId;
    private String name;
    private int age;
    private Subject subject;

    public ReturnTeacherDTO() {
    }

    public ReturnTeacherDTO(int teacherId, String name, int age, Subject subject) {
        this.teacherId = teacherId;
        this.name = name;
        this.age = age;
        this.subject = subject;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }
}
