package com.example.javapruefung.dto;


import com.example.javapruefung.enumerations.Subject;

public class TeacherDTO {
    private String name;
    private int age;
    private Subject subject;

    public TeacherDTO() {
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


}
