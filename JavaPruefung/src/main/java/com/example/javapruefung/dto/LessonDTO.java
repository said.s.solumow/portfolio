package com.example.javapruefung.dto;

import com.example.javapruefung.enumerations.Subject;

public class LessonDTO {

    private String startDate;
    private int duration;
    private Subject subject;
    private int teacherId;

    public LessonDTO() {
    }

    public LessonDTO(String startDate, int duration, Subject subject, int teacherId) {
        this.startDate = startDate;
        this.duration = duration;
        this.subject = subject;
        this.teacherId = teacherId;
    }


    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }
}
