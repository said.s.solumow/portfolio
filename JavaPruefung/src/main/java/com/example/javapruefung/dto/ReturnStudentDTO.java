package com.example.javapruefung.dto;

public class ReturnStudentDTO {
    private int studentId;
    private String name;
    private int age;
    private String socialSecurityNumber;
    private int lessonId;

    public ReturnStudentDTO() {
    }

    public ReturnStudentDTO(int studentId, String name, int age, String socialSecurityNumber, int lessonId) {
        this.studentId = studentId;
        this.name = name;
        this.age = age;
        this.socialSecurityNumber = socialSecurityNumber;
        this.lessonId = lessonId;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public int getLessonId() {
        return lessonId;
    }

    public void setLessonId(int lessonId) {
        this.lessonId = lessonId;
    }
}
