package com.example.javapruefung.dto;

public class StudentDTO {
    private String name;
    private int age;
    private String socialSecurityNumber;
    private int lessonId;

    public StudentDTO() {
    }

    public StudentDTO(String name, int age, String socialSecurityNumber, int lessonId) {
        this.name = name;
        this.age = age;
        this.socialSecurityNumber = socialSecurityNumber;
        this.lessonId = lessonId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public int getLessonId() {
        return lessonId;
    }

    public void setLessonId(int lessonId) {
        this.lessonId = lessonId;
    }
}
