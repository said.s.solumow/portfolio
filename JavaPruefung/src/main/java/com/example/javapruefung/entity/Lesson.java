package com.example.javapruefung.entity;

import jakarta.persistence.*;

import com.example.javapruefung.enumerations.Subject;
import java.util.List;

@Entity
@Table(name = "Einheit")
public class Lesson {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int lessonId;
    @Column(name = "Start_Date")
    private String startDate;
    @Column(name = "Dauer")
    private int duration;
    @Enumerated(EnumType.STRING)
    private Subject subject;
    @OneToOne
    @JoinColumn(name = "teacherId")
    private Teacher teacher;
    @OneToMany(mappedBy = "lesson")
    private List<Student> studentList;


    public Lesson() {
    }

    public Lesson(int lessonId, String startDate, int duration, Subject subject, Teacher teacher, List<Student> studentList) {
        this.lessonId = lessonId;
        this.startDate = startDate;
        this.duration = duration;
        this.subject = subject;
        this.teacher = teacher;
        this.studentList = studentList;
    }

    public Lesson(String startDate, int duration, Subject subject, Teacher teacher) {
        this.startDate = startDate;
        this.duration = duration;
        this.subject = subject;
        this.teacher = teacher;
    }

    public int getLessonId() {
        return lessonId;
    }

    public void setLessonId(int lessonId) {
        this.lessonId = lessonId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }
}
