package com.example.javapruefung.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "Schüler")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int studentId;
    @Column(name = "Name")
    private String name;
    @Column(name = "Alter")
    private int age;
    @Column(name = "SVN")
    private String socialSecurityNumber;
    @ManyToOne
    @JoinColumn(name = "lessonId")
    private Lesson lesson;

    public Student() {
    }

    public Student(int studentId, String name, int age, String socialSecurityNumber, Lesson lesson) {
        this.studentId = studentId;
        this.name = name;
        this.age = age;
        this.socialSecurityNumber = socialSecurityNumber;
        this.lesson = lesson;
    }

    public Student(String name, int age, String socialSecurityNumber, Lesson lesson) {
        this.name = name;
        this.age = age;
        this.socialSecurityNumber = socialSecurityNumber;
        this.lesson = lesson;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }
}
