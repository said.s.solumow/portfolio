package com.example.javapruefung.entity;

import jakarta.persistence.*;
import com.example.javapruefung.enumerations.Subject;


@Entity
@Table(name = "Lehrer")
public class Teacher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int teacherId;
    @Column(name = "Name")
    private String name;
    @Column(name = "Alter")
    private int age;
    @Enumerated(EnumType.STRING)
    private Subject subject;
    @OneToOne(mappedBy = "teacher")
    private Lesson lesson;

    public Teacher() {
    }

    public Teacher(String name, int age, Subject subject) {
        this.name = name;
        this.age = age;
        this.subject = subject;
    }

    public Teacher(int teacherId, String name, int age, Subject subject, Lesson lesson) {
        this.teacherId = teacherId;
        this.name = name;
        this.age = age;
        this.subject = subject;
        this.lesson = lesson;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }
}
