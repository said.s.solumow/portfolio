# MyProjects


## Hello and welcome!

<p>
My name is Said and I am currently a junior fullstack software developer in training at CODERS.BAY.
It has always been my dream to be a software developer and every step I took in my life, I took to achieve it and to live my dream.

Developing software, thinking about creative solutions and coding gives me something of a thrill that is hard to put into words.
The world of software development is a fascinating realm where imagination meets logic, and I find immense joy in creating functional 
and innovative solutions through code. There is a unique sense of accomplishment when I see my code come to life and solve real-world problems.
The ability to translate ideas into tangible software, to build something from scratch and witness it making a positive impact,
is incredibly rewarding.
Moreover, the dynamic nature of software development keeps me engaged and motivated.
The ever-evolving technologies and frameworks provide endless opportunities for learning and growth.
Each project presents a new challenge, pushing me to expand my knowledge and sharpen my problem-solving skills.
But it's not just about the technical aspect. Collaboration and teamwork are vital in this field.
Working alongside talented individuals, sharing ideas, and collectively striving towards a common goal is a truly enriching experience.
I am grateful for the opportunity to be a part of the software development community and to embark on this exciting journey at CODERS.BAY.
I am committed to honing my skills, staying up-to-date with the latest advancements, and making a meaningful contribution to the world of software development.

I have just started, and I strive to become the best.

Let's dive into the world of coding together and create something extraordinary!
</p>


## Technologies used for the projects

- [ ] Spring Boot JPA & Hibernate -JAVA
- [ ] Vue.js HTML/CSS/JS/Vue-Router/Vuetify/Bootstrap


## Projects

- [ ] JAVA Backend administration software
<p>
Recently, I had an examination in JAVA where my knowledge of Spring Boot and JAVA was assessed. 
The task was to implement nine features for the management software. We had a time limit of three hours for this assignment.
</p>

- [ ] Vue.js frontend WebShop (coming soon 14.07)
<p>
Currently, I am actively working on a project in Vue.js, focusing on building a webshop. 
This project serves as an excellent opportunity for me to expand my knowledge and demonstrate my skills in frontend development. 
I am excited to dive deep into Vue.js and leverage its powerful features to create a seamless and visually appealing user experience. 
Through this project, I aim to showcase my ability to design and implement robust and efficient frontend solutions while continuously learning and improving my Vue.js proficiency.
</p>

- [ ] JAVA/Vue.js Iperia -Software (coming soon 21.07)
<p>
For my thesis, I chose a project management tool similar to Jira. For this project, I used Vue.js in the frontend and Java Spring Boot & Spring Security in the backend. 
I am currently still working on it, but a significant portion is already completed, allowing for a navigation through the project.
</p>


## Contact me

<p>

If you find value in what you see or are intrigued by the prospect of including an ambitious and motivated junior software 
developer in your team, I invite you to reach out to me. I would be delighted to explore opportunities for collaboration and 
contribute my skills and dedication to your organization's success. Please feel free to contact me, and I look forward to 
discussing how I can positively impact your team with my passion for software development.
</p>

- [ ] E-Mail: solumow.said@gmail.com
- [ ] Tel.: +43 660 116 96 71
- [ ] LinkedIn: https://www.linkedin.com/in/said-solumow/
- [ ] My Website: https://www.solumowsaid.com


# Languages and Tools

### Web 
<p align="left">
<a href="https://www.w3.org/html/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="40" height="40"/> </a> 
<a href="https://www.w3schools.com/css/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original-wordmark.svg" alt="css3" width="40" height="40"/> </a> 
<a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="40" height="40"/> </a> 
<a href="https://vuejs.org/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/vuejs/vuejs-original-wordmark.svg" alt="vuejs" width="40" height="40"/> </a> 
<a href="https://getbootstrap.com" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/bootstrap/bootstrap-plain-wordmark.svg" alt="bootstrap" width="40" height="40"/> </a> 
</p>


### Java
<p align="left">
<a href="https://www.java.com" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/java/java-original.svg" alt="java" width="40" height="40"/> </a> 
<a href="https://spring.io/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/springio/springio-icon.svg" alt="spring" width="40" height="40"/> </a> 
<a href="https://postman.com" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/getpostman/getpostman-icon.svg" alt="postman" width="40" height="40"/> </a> 
</p>


### Database
<p align="left">
<a href="https://www.mysql.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mysql/mysql-original-wordmark.svg" alt="mysql" width="40" height="40"/> </a> 
</p>

### Version Control
<p align="left">
<a href="https://git-scm.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="40" height="40"/> </a>
</p>

### Design and Prototyping
<p align="left">
<a href="https://www.photoshop.com/en" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/photoshop/photoshop-line.svg" alt="photoshop" width="40" height="40"/> </a>
</p>