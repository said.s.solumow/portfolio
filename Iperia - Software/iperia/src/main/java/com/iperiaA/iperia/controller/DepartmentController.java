package com.iperiaA.iperia.controller;

import com.iperiaA.iperia.dtos.DepartmentDTO;
import com.iperiaA.iperia.services.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/department")
public class DepartmentController {

    @Autowired
    DepartmentService departmentService;

    @GetMapping
    public ResponseEntity<?> getDepartments() {
        try {
            return departmentService.getDepartments();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    //DOES NOT WORK CORRECTLY
    @PutMapping("/update/{departmentName}")
    public ResponseEntity<?> updateDepartment(@PathVariable String departmentName, @RequestBody DepartmentDTO departmentDTO) {
        try {
            return departmentService.updateDepartment(departmentName, departmentDTO);
        }catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @DeleteMapping("{departmentName}")
    public ResponseEntity<?> deleteDepartment(@PathVariable String departmentName) {
        try {
            return departmentService.deleteDepartment(departmentName);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }
}
