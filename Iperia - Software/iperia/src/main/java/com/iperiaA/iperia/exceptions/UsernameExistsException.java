package com.iperiaA.iperia.exceptions;

public class UsernameExistsException extends RuntimeException{


    public UsernameExistsException(String message) {
        super(message);
    }
}
