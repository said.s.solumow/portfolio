package com.iperiaA.iperia.entities;

import com.iperiaA.iperia.enums.Priority;
import com.iperiaA.iperia.enums.TicketStatus;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "tickets")
@Getter
@Setter
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int ticketId;
    @Column(nullable = false)
    private String title;
    @Column(nullable = false)
    private String description;
    private String createdAt;
    private String resolvedAt;
    @Enumerated(EnumType.STRING)
    private Priority priority;
    @Enumerated(EnumType.STRING)
    private TicketStatus ticketStatus;
    @OneToMany(mappedBy = "ticket")
    private List<FileMetaData> files;
    @ManyToOne
    @JoinColumn(name = "createdByUserId")
    private User createdBy;
    @ManyToOne
    @JoinColumn(name = "resolvedByUserId")
    private User resolvedBy;
    @ManyToOne
    @JoinColumn(name = "departmentId")
    private Department department;
    @OneToOne
    @JoinColumn(name = "commentId")
    private Comment comment;

    public Ticket() {
    }

    public Ticket(String title, String description, String createdAt, Priority priority, TicketStatus ticketStatus, User createdBy, Department department) {
        this.title = title;
        this.description = description;
        this.createdAt = createdAt;
        this.priority = priority;
        this.ticketStatus = ticketStatus;
        this.createdBy = createdBy;
        this.department = department;
    }

    public Ticket(String title, String description, String createdAt, String resolvedAt, Priority priority, TicketStatus ticketStatus, User createdBy, User resolvedBy, Department department, Comment comment) {
        this.title = title;
        this.description = description;
        this.createdAt = createdAt;
        this.resolvedAt = resolvedAt;
        this.priority = priority;
        this.ticketStatus = ticketStatus;
        this.createdBy = createdBy;
        this.resolvedBy = resolvedBy;
        this.department = department;
        this.comment = comment;
    }

    public Ticket(int ticketId, String title, String description, String createdAt, String resolvedAt, Priority priority, TicketStatus ticketStatus, List<FileMetaData> files, User createdBy, User resolvedBy, Department department, Comment comment) {
        this.ticketId = ticketId;
        this.title = title;
        this.description = description;
        this.createdAt = createdAt;
        this.resolvedAt = resolvedAt;
        this.priority = priority;
        this.ticketStatus = ticketStatus;
        this.files = files;
        this.createdBy = createdBy;
        this.resolvedBy = resolvedBy;
        this.department = department;
        this.comment = comment;
    }
}
