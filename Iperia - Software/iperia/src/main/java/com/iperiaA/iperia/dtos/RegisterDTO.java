package com.iperiaA.iperia.dtos;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RegisterDTO {

    private String username;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String birthday;
    private String role;
    private String department;
    private String managedDepartment;

}
