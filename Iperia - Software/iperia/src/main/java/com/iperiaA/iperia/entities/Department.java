package com.iperiaA.iperia.entities;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Table(name = "departments")
@Builder
@Getter
@Setter
public class Department {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int departmentId;
    private String departmentName;
    @OneToMany(mappedBy = "department") //Relation for User to get DepartmentID
    private List<User> users;
    @OneToOne(mappedBy = "managedDepartment")//Relation for Department to get ManagerID
    private User manager;
    @OneToMany(mappedBy = "department")
    private List<Ticket> tickets;

    public Department() {
    }

    public Department(String departmentName) {
        this.departmentName = departmentName;
    }

    public Department(int departmentId, String departmentName, List<User> users, User manager) {
        this.departmentId = departmentId;
        this.departmentName = departmentName;
        this.users = users;
        this.manager = manager;
    }

    public Department(int departmentId, String departmentName, List<User> users, User manager, List<Ticket> tickets) {
        this.departmentId = departmentId;
        this.departmentName = departmentName;
        this.users = users;
        this.manager = manager;
        this.tickets = tickets;
    }
}
