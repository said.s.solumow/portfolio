package com.iperiaA.iperia.dtos;

import com.iperiaA.iperia.entities.UserRole;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserDTO {

    private int userId;
    private String username;
    private String email;
    private String role;

    private String jwt;
}
