package com.iperiaA.iperia.dtos;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DepartmentResponseDTO {

    private int departmentId;

    private String departmentName;
}
