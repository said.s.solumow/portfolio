package com.iperiaA.iperia.services;

import com.iperiaA.iperia.dtos.MessageRequestDTO;
import com.iperiaA.iperia.dtos.MessageResponseDTO;
import com.iperiaA.iperia.entities.Message;
import com.iperiaA.iperia.entities.User;
import com.iperiaA.iperia.repositories.MessageRepository;
import com.iperiaA.iperia.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MessageService {

    @Autowired
    TicketService ticketService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AdminService adminService;


    @Autowired
    MessageRepository messageRepository;
    public ResponseEntity<?> postMessage(int receiverId, MessageRequestDTO messageRequestDTO) {
        String timestamp = ticketService.getTimestampString();
        User sender = adminService.getUserWithId(messageRequestDTO.getSenderId());
        User receiver = adminService.getUserWithId(receiverId);

        Message message = new Message(
                messageRequestDTO.getMessageContent(),
                timestamp,
                sender,
                receiver);

        messageRepository.save(message);

        MessageResponseDTO messageResponseDTO = new MessageResponseDTO(
                message.getMessageId(),
                message.getMessageContent(),
                message.getTimestamp(),
                message.getSender().getUserId(),
                message.getReceiver().getUserId());

        return ResponseEntity.status(HttpStatus.OK).body(messageResponseDTO);
    }

    public ResponseEntity<?> getMessages(int receiverId, int senderId) {
        User receiver = adminService.getUserWithId(receiverId);
        User sender = adminService.getUserWithId(senderId);
        Iterable<Message> messagesIterable = messageRepository.findAllByReceiverAndSender(receiver, sender);
        List<MessageResponseDTO> messageResponseDTOList = new ArrayList<>();
        for (Message message : messagesIterable) {
            MessageResponseDTO messageResponseDTO = new MessageResponseDTO(
                    message.getMessageId(),
                    message.getMessageContent(),
                    message.getTimestamp(),
                    message.getSender().getUserId(),
                    message.getReceiver().getUserId());
            messageResponseDTOList.add(messageResponseDTO);
        }
        return ResponseEntity.status(HttpStatus.OK).body(messageResponseDTOList);
    }
}
