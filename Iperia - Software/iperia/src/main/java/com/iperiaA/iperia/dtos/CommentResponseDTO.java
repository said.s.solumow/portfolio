package com.iperiaA.iperia.dtos;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CommentResponseDTO {

    private int commentId;
    private String commentText;
    private int authorId;
}
