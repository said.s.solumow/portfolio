package com.iperiaA.iperia.dtos;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserDataDTO {
    private int userId;
    private String username;
    private String email;
    private String firstName;
    private String lastName;
    private String birthday;
    private String department;
    private String role;
    private String managedDepartment;
}
