package com.iperiaA.iperia.repositories;

import com.iperiaA.iperia.entities.Department;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepository extends CrudRepository<Department, Integer> {

    Department findByDepartmentName(String departmentName);
}
