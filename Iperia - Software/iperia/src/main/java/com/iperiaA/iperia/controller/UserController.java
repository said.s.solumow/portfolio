package com.iperiaA.iperia.controller;

import com.iperiaA.iperia.dtos.PasswordDTO;
import com.iperiaA.iperia.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping
    public ResponseEntity<?> getAllUsers(int page) {
        try {
            return userService.getAllUsers(page);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @GetMapping("/all")
    public ResponseEntity<?> getUsers() {
        try {
            return userService.getUsers();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @GetMapping("{userId}")
    public ResponseEntity<?> getUser(@PathVariable int userId) {
        try {
            return userService.getUser(userId);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PutMapping("{userId}")
    public ResponseEntity<?> changePassword(@PathVariable int userId, @RequestBody PasswordDTO passwordDTO) {
        try {
            return userService.changePassword(userId, passwordDTO);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }
}
