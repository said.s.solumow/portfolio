package com.iperiaA.iperia.services;

import com.iperiaA.iperia.dtos.PasswordDTO;
import com.iperiaA.iperia.dtos.UserDataDTO;
import com.iperiaA.iperia.entities.User;
import com.iperiaA.iperia.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    AdminService adminService;

    public ResponseEntity<?> getAllUsers(int page) {
        List<UserDataDTO> userList = new ArrayList<>();
        Pageable limit = PageRequest.of(page -1, 15);
        Iterable<User> userIterable = userRepository.findAll(limit);

        UserDataDTO userDataDTO;
        for (User user : userIterable) {
            if (user.getRole().getName().equals("MANAGER")) {
                userDataDTO = new UserDataDTO(
                        user.getUserId(),
                        user.getUsername(),
                        user.getEmail(),
                        user.getFirstName(),
                        user.getLastName(),
                        user.getBirthday(),
                        user.getDepartment().getDepartmentName(),
                        user.getRole().getName(),
                        user.getManagedDepartment().getDepartmentName());
                userList.add(userDataDTO);
            } else {
                userDataDTO = new UserDataDTO(
                        user.getUserId(),
                        user.getUsername(),
                        user.getEmail(),
                        user.getFirstName(),
                        user.getLastName(),
                        user.getBirthday(),
                        user.getDepartment().getDepartmentName(),
                        user.getRole().getName(),
                        null);
                userList.add(userDataDTO);
            }
        }
        return ResponseEntity.status(HttpStatus.OK).body(userList);
    }

    public ResponseEntity<?> getUsers() {
        List<UserDataDTO> userList = new ArrayList<>();
        Iterable<User> userIterable = userRepository.findAll();

        UserDataDTO userDataDTO;
        for (User user : userIterable) {
            if (user.getRole().getName().equals("MANAGER")) {
                userDataDTO = new UserDataDTO(
                        user.getUserId(),
                        user.getUsername(),
                        user.getEmail(),
                        user.getFirstName(),
                        user.getLastName(),
                        user.getBirthday(),
                        user.getDepartment().getDepartmentName(),
                        user.getRole().getName(),
                        user.getManagedDepartment().getDepartmentName());
                userList.add(userDataDTO);
            } else {
                userDataDTO = new UserDataDTO(
                        user.getUserId(),
                        user.getUsername(),
                        user.getEmail(),
                        user.getFirstName(),
                        user.getLastName(),
                        user.getBirthday(),
                        user.getDepartment().getDepartmentName(),
                        user.getRole().getName(),
                        null);
                userList.add(userDataDTO);
            }
        }
        return ResponseEntity.status(HttpStatus.OK).body(userList);
    }

    public ResponseEntity<?> getUser(int userId) {
        User user = adminService.getUserWithId(userId);

        UserDataDTO userDataDTO;

        if (user.getRole().getName().equals("MANAGER")) {
            userDataDTO = adminService.constructUserDataDTOIfManager(user);
        } else {
            userDataDTO = adminService.constructUserDataDTO(user);
        }
        return ResponseEntity.status(HttpStatus.OK).body(userDataDTO);
    }

    public ResponseEntity<?> changePassword(int userId, PasswordDTO passwordDTO) {
        Optional<User> user = userRepository.findById(userId);

        if (user.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        User updatedUser = user.get();

        String hashedPassword = passwordEncoder.encode(passwordDTO.getNewPassword());

        updatedUser.setPassword(hashedPassword);

        userRepository.save(updatedUser);

        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
