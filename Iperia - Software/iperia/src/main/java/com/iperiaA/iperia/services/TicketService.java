package com.iperiaA.iperia.services;

import com.iperiaA.iperia.dtos.*;
import com.iperiaA.iperia.entities.*;
import com.iperiaA.iperia.enums.Priority;
import com.iperiaA.iperia.enums.TicketStatus;
import com.iperiaA.iperia.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TicketService {

    @Autowired
    TicketRepository ticketRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    FileMetaDataRepository fileMetaDataRepository;

    @Autowired
    CommentRepository commentRepository;


    public ResponseEntity<?> addComment(int ticketId, CommentDTO commentDTO) {
        Ticket ticket = getTicketWithId(ticketId);
        User author = getUserWithId(commentDTO.getAuthorId());

        Comment comment = new Comment(
                commentDTO.getCommentText(),
                author);
        commentRepository.save(comment);

        ticket.setComment(comment);
        ticketRepository.save(ticket);

        CommentResponseDTO commentResponseDTO = new CommentResponseDTO(
                comment.getCommentId(),
                comment.getCommentText(),
                comment.getAuthor().getUserId());

        return ResponseEntity.status(HttpStatus.OK).body(commentResponseDTO);
    }

    public ResponseEntity<?> createTicket(String username, TicketDTO ticketDTO) {
        User createdBy = userRepository.findByUsername(username);
        Department department = departmentRepository.findByDepartmentName(ticketDTO.getDepartment());
        Priority priority = getPriority(ticketDTO);
        String createdAt = getTimestampString();

        Ticket ticket = new Ticket(
                ticketDTO.getTitle(),
                ticketDTO.getDescription(),
                createdAt,
                priority,
                TicketStatus.OPEN,
                createdBy,
                department);
        ticketRepository.save(ticket);

        TicketResponseDTO ticketResponseDTO = constructTicketResponseDTO(ticket, null);

        return ResponseEntity.status(HttpStatus.CREATED).body(ticketResponseDTO);
    }

    public ResponseEntity<?> deleteTicket(int ticketId) {
        ticketRepository.deleteById(ticketId);
        return ResponseEntity.status(HttpStatus.OK).build();

    }

    public ResponseEntity<?> getAllTickets() {
        List<TicketResponseDTO> ticketList = new ArrayList<>();
        Iterable<Ticket> tickets = ticketRepository.findAll();

        return getTicketsWithOrWithoutResolvedBy(ticketList, tickets);
    }

    public ResponseEntity<?> getTickets(int page) {
        List<TicketResponseDTO> ticketList = new ArrayList<>();
        Pageable limit = PageRequest.of(page -1, 30);
        Iterable<Ticket> tickets = ticketRepository.findAll(limit);

        return getTicketsWithOrWithoutResolvedBy(ticketList, tickets);
    }

    public ResponseEntity<?> getTicket(int ticketId) {
        Ticket ticket = getTicketOptional(ticketId);

        String fileName = getFilenameWithTicketId(ticketId);

        String resolvedBy = getResolvedByWithTicket(ticket);

        String comment = getCommentWithTicket(ticket);

        SingleTicketDTO singleTicketDTO = constructSingleTicketDTO(
                ticket,
                fileName,
                resolvedBy,
                comment);

        return ResponseEntity.status(HttpStatus.OK).body(singleTicketDTO);

    }

    public ResponseEntity<?> removeTicketFileRelation(String fileName) {
        FileMetaData file = fileMetaDataRepository.findByFileName(fileName);
        file.setTicket(null);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    public ResponseEntity<?> putTicketFileRelation(String fileName, TicketDTO ticketDTO) {
        Ticket ticket = ticketRepository.findByTitle(ticketDTO.getTitle());
        FileMetaData file = fileMetaDataRepository.findByFileName(fileName);

        // Delete the existing relation, if any
        if (file.getTicket() != null) {
            file.setTicket(null);
        }

        file.setTicket(ticket);
        fileMetaDataRepository.save(file);

        FileResponseDTO fileResponseDTO = new FileResponseDTO(
                file.getFileId(),
                file.getTicket().getTicketId(),
                file.getFileName(),
                file.getFileUrl());
        return ResponseEntity.status(HttpStatus.OK).body(fileResponseDTO);
    }

    /*public ResponseEntity<?> putTicketFileRelation(String fileName, TicketDTO ticketDTO) {
        Ticket ticket = ticketRepository.findByTitle(ticketDTO.getTitle());

        FileMetaData file = fileMetaDataRepository.findByFileName(fileName);

        file.setTicket(ticket);

        fileMetaDataRepository.save(file);

        FileResponseDTO fileResponseDTO = new FileResponseDTO(
                file.getFileId(),
                file.getTicket().getTicketId(),
                file.getFileName(),
                file.getFileUrl());
        return ResponseEntity.status(HttpStatus.OK).body(fileResponseDTO);
    }*/

    public ResponseEntity<?> acceptTicket(int ticketId, TicketStatusDTO ticketStatusDTO) {
        Ticket acceptedTicket = getTicketWithId(ticketId);

        TicketStatus ticketStatus = TicketStatus.valueOf(ticketStatusDTO.getTicketStatus());

        acceptedTicket.setTicketStatus(ticketStatus);

        User user = getUserWithId(ticketStatusDTO.getUserId());

        acceptedTicket.setResolvedBy(user);

        ticketRepository.save(acceptedTicket);

        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }

    public ResponseEntity<?> returnAcceptedTicket(int ticketId, TicketStatusDTO ticketStatusDTO) {
        Ticket ticket = getTicketWithId(ticketId);
        ticket.setTicketStatus(TicketStatus.OPEN);
        ticket.setResolvedBy(null);
        ticketRepository.save(ticket);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    public ResponseEntity<?> finishTicket(int ticketId, TicketStatusDTO ticketStatusDTO) {
        Ticket ticket = getTicketWithId(ticketId);
        ticket.setTicketStatus(TicketStatus.RESOLVED);
        User user = getUserWithId(ticketStatusDTO.getUserId());
        ticket.setResolvedBy(user);
        String resolvedAt = getTimestampString();
        ticket.setResolvedAt(resolvedAt);
        ticketRepository.save(ticket);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    public ResponseEntity<?> problemTicket(int ticketId, TicketStatusDTO ticketStatusDTO) {
        Ticket ticket = getTicketWithId(ticketId);
        ticket.setTicketStatus(TicketStatus.PROBLEM);
        User user = getUserWithId(ticketStatusDTO.getUserId());
        ticket.setResolvedBy(user);
        String resolvedAt = getTimestampString();
        ticket.setResolvedAt(resolvedAt);
        ticketRepository.save(ticket);
        return ResponseEntity.status(HttpStatus.OK).build();
    }









    //BAUSTEINE
    public SingleTicketDTO constructSingleTicketDTO(Ticket ticket, String fileName, String resolvedBy, String comment) {
        return new SingleTicketDTO(
                ticket.getTicketId(),
                ticket.getTitle(),
                ticket.getDescription(),
                ticket.getCreatedAt(),
                ticket.getResolvedAt(),
                ticket.getPriority().name(),
                ticket.getTicketStatus().name(),
                fileName,
                ticket.getCreatedBy().getUsername(),
                resolvedBy,
                ticket.getDepartment().getDepartmentName(),
                comment);
    }

    public String getResolvedByWithTicket(Ticket ticket) {
        String resolvedBy;
        if (ticket.getResolvedBy() == null) {
            resolvedBy = null;
        } else {
            resolvedBy = ticket.getResolvedBy().getUsername();
        }
        return resolvedBy;
    }

    public String getCommentWithTicket(Ticket ticket) {
        String comment;
        if (ticket.getComment() != null) {
            comment = ticket.getComment().getCommentText();
        } else {
            comment = null;
        }
        return comment;
    }
    public Ticket getTicketOptional(int ticketId) {
        Optional<Ticket> ticketOptional = ticketRepository.findById(ticketId);
        return ticketOptional.orElse(null);
    }

    public String getFilenameWithTicketId(int ticketId) {
        FileMetaData file = fileMetaDataRepository.findByTicket_TicketId(ticketId);
        String fileName;
        if (file != null) {
            fileName = file.getFileName();
        } else {
            fileName = "";
        }
        return fileName;
    }
    public Ticket getTicketWithId(int ticketId) {
        Optional<Ticket> ticket = ticketRepository.findById(ticketId);
        return ticket.orElse(null);
    }

    public User getUserWithId(int userId) {
        Optional<User> userOptional = userRepository.findById(userId);
        return userOptional.orElse(null);
    }

    public Priority getPriority(TicketDTO ticketDTO) {
        Priority priority = null;

        switch (ticketDTO.getPriority()) {
            case "VERY_IMPORTANT":
                return priority = Priority.VERY_IMPORTANT;
            case "IMPORTANT":
                return priority = Priority.IMPORTANT;
            case "MODERATE":
                return priority = Priority.MODERATE;
            case "LESS_IMPORTANT":
                return priority = Priority.LESS_IMPORTANT;
            default:
                return null;
        }
    }

    public TicketResponseDTO constructTicketResponseDTO(Ticket ticket, String resolvedBy) {
        return new TicketResponseDTO(
                ticket.getTicketId(),
                ticket.getTitle(),
                ticket.getDescription(),
                ticket.getCreatedAt(),
                ticket.getResolvedAt(),
                ticket.getPriority().name(),
                ticket.getTicketStatus().name(),
                ticket.getCreatedBy().getUsername(),
                resolvedBy,
                ticket.getDepartment().getDepartmentName());
    }

    public String getDateString() {
        LocalDate localDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return localDate.format(formatter);
    }

    public String getTimestampString() {
        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return localDateTime.format(formatter);
    }

    private ResponseEntity<?> getTicketsWithOrWithoutResolvedBy(List<TicketResponseDTO> ticketList, Iterable<Ticket> tickets) {
        for (Ticket ticket : tickets) {
            TicketResponseDTO ticketResponseDTO;
            if (ticket.getResolvedBy() != null) {
                ticketResponseDTO = constructTicketResponseDTO(ticket, ticket.getResolvedBy().getUsername());

            } else {
                ticketResponseDTO = constructTicketResponseDTO(ticket, null);
            }
            ticketList.add(ticketResponseDTO);
        }
        return ResponseEntity.status(HttpStatus.OK).body(ticketList);
    }
}
