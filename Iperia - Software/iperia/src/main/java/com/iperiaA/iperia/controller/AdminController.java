package com.iperiaA.iperia.controller;

import com.iperiaA.iperia.dtos.DepartmentDTO;
import com.iperiaA.iperia.dtos.RegisterDTO;
import com.iperiaA.iperia.dtos.UpdateUserDTO;
import com.iperiaA.iperia.exceptions.EmailExistsException;
import com.iperiaA.iperia.exceptions.GlobalExceptionHandler;
import com.iperiaA.iperia.exceptions.UsernameExistsException;
import com.iperiaA.iperia.services.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/admin")
@CrossOrigin(origins = "*", methods = {RequestMethod.POST, RequestMethod.PUT, RequestMethod.GET, RequestMethod.DELETE},
        allowedHeaders = {"Content-Type", "Authorization"})
public class AdminController {


    @Autowired
    AdminService adminService;

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody RegisterDTO registerDTO) {
        try {
            return adminService.register(registerDTO);

        } catch (DataIntegrityViolationException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
        }
    }

    @PutMapping("{userId}")
    public ResponseEntity<?> updateUserData(@PathVariable int userId, @RequestBody UpdateUserDTO userDTO) {
        try {
            return adminService.updateUserData(userId, userDTO);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @DeleteMapping("{userId}")
    public ResponseEntity<?> deleteUser(@PathVariable int userId) {
        try {
            return adminService.deleteUser(userId);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.OK).body(e.getMessage());
        }
    }

    @PostMapping("/department")
    public ResponseEntity<?> createDepartment(@RequestBody DepartmentDTO departmentDTO) {
        try {
            return adminService.createDepartment(departmentDTO);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }



}
