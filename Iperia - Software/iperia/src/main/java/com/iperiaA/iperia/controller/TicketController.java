package com.iperiaA.iperia.controller;


import com.iperiaA.iperia.dtos.CommentDTO;
import com.iperiaA.iperia.dtos.TicketStatusDTO;
import com.iperiaA.iperia.dtos.TicketDTO;
import com.iperiaA.iperia.services.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/ticket")
/*@CrossOrigin(origins = "*", methods = {RequestMethod.POST, RequestMethod.PUT, RequestMethod.GET, RequestMethod.DELETE},
        allowedHeaders = {"Content-Type", "Authorization"})*/
public class TicketController {

    @Autowired
    TicketService ticketService;

    @PostMapping("{username}")
    public ResponseEntity<?> createTicket(@PathVariable String username, @RequestBody TicketDTO ticketDTO) {
        try {
            return ticketService.createTicket(username, ticketDTO);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PutMapping("{fileName}")
    public ResponseEntity<?> putTicketFileRelation(@PathVariable String fileName, @RequestBody TicketDTO ticketDTO) {
        try {
            return ticketService.putTicketFileRelation(fileName, ticketDTO);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @DeleteMapping("/remove/{fileName}")
    public ResponseEntity<?> removeTicketFileRelation(@PathVariable String fileName) {
        try {
            return ticketService.removeTicketFileRelation(fileName);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @GetMapping
    public ResponseEntity<?> getTickets(int page) {
        try {
            return ticketService.getTickets(page);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAllTickets() {
        try {
            return ticketService.getAllTickets();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @GetMapping("{ticketId}")
    public ResponseEntity<?> getTicket(@PathVariable int ticketId) {
        try {
            return ticketService.getTicket(ticketId);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @DeleteMapping("{ticketId}")
    public ResponseEntity<?> deleteTicket(@PathVariable int ticketId) {
        try {
            return ticketService.deleteTicket(ticketId);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PutMapping("/status/{ticketId}")
    public ResponseEntity<?> acceptTicket(@PathVariable int ticketId, @RequestBody TicketStatusDTO ticketStatusDTO) {
        try {
            return ticketService.acceptTicket(ticketId, ticketStatusDTO);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PutMapping("/status/return/{ticketId}")
    public ResponseEntity<?> returnAcceptedTicket(@PathVariable int ticketId, @RequestBody TicketStatusDTO ticketStatusDTO) {
        try {
            return ticketService.returnAcceptedTicket(ticketId, ticketStatusDTO);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PutMapping("/status/finish/{ticketId}")
    public ResponseEntity<?> finishTicket(@PathVariable int ticketId, @RequestBody TicketStatusDTO ticketStatusDTO) {
        try {
            return ticketService.finishTicket(ticketId, ticketStatusDTO);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PutMapping("/status/problem/{ticketId}")
    public ResponseEntity<?> problemTicket(@PathVariable int ticketId, @RequestBody TicketStatusDTO ticketStatusDTO) {
        try {
            return ticketService.problemTicket(ticketId, ticketStatusDTO);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PostMapping("/{ticketId}/comment")
    public ResponseEntity<?> addComment(@PathVariable int ticketId, @RequestBody CommentDTO commentDTO) {
        try {
            return ticketService.addComment(ticketId, commentDTO);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }
}
