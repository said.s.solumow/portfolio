package com.iperiaA.iperia.repositories;

import com.iperiaA.iperia.entities.UserRole;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRoleRepository extends CrudRepository<UserRole, Integer> {

    UserRole findByName(String userRole);
}
