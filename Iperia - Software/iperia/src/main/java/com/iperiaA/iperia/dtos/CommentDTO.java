package com.iperiaA.iperia.dtos;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CommentDTO {

    private String commentText;
    private int authorId;
}
