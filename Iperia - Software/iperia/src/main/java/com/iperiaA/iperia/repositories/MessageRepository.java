package com.iperiaA.iperia.repositories;

import com.iperiaA.iperia.entities.Message;
import com.iperiaA.iperia.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {

    Iterable<Message> findAllByReceiverAndSender(User receiver, User sender);
}
