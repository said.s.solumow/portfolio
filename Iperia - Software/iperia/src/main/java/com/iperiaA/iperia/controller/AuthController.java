package com.iperiaA.iperia.controller;

import com.iperiaA.iperia.dtos.*;
import com.iperiaA.iperia.entities.User;
import com.iperiaA.iperia.repositories.DepartmentRepository;
import com.iperiaA.iperia.repositories.UserRepository;
import com.iperiaA.iperia.repositories.UserRoleRepository;
import com.iperiaA.iperia.services.AdminService;
import com.iperiaA.iperia.services.AuthService;
import com.iperiaA.iperia.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/auth")
@CrossOrigin(origins = "*", methods = {RequestMethod.POST, RequestMethod.PUT, RequestMethod.GET, RequestMethod.DELETE},
        allowedHeaders = {"Content-Type", "Authorization"})
public class AuthController {


    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserRoleRepository userRoleRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtUtil jwtUtil;

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    AuthService authService;

    @Autowired
    AdminService adminService;

    @PostMapping
    public ResponseEntity<?> auth(@RequestBody AuthRequestDTO authRequestDTO) {
        try {
            return authService.auth(authRequestDTO);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @GetMapping
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> checkIfLoggedIn() {
        try {
            return authService.checkIfLoggedIn();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }


}
