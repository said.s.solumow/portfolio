package com.iperiaA.iperia.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "users")
@Getter
@Setter
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int userId;

    @Column(unique = true, nullable = false)
    private String username;
    @Column(unique = true)
    private String email;
    @Column(nullable = false)
    private String password;
    private String firstName;
    private String lastName;
    private String birthday;
    @ManyToOne
    @JoinColumn(name = "role_name")
    private UserRole role;
    @ManyToOne
    @JoinColumn(name = "departmentId") //Relation for User to get DepartmentID
    private Department department;
    @OneToOne
    @JoinColumn(name = "managedDepartmentId")//Relation for Department to get ManagerID
    private Department managedDepartment;
    @OneToMany(mappedBy = "createdBy")
    private List<Ticket> createdTickets;

    @OneToMany(mappedBy = "resolvedBy")
    private List<Ticket> resolvedTickets;
    @OneToMany(mappedBy = "author")
    private List<Comment> comments;

    @OneToMany(mappedBy = "sender")
    private List<Message> messagesSent;
    @OneToMany(mappedBy = "receiver")
    private List<Message> messagesReceived;


    public User() {
    }



    public User(String username, String email, String password, UserRole role) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.role = role;
    }



    public User(String username, String email, String password, String firstName, String lastName, String birthday, UserRole role, Department department) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.role = role;
        this.department = department;
    }

    public User(String username, String email, String password, String firstName, String lastName, String birthday, UserRole role, Department department, Department managedDepartment) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.role = role;
        this.department = department;
        this.managedDepartment = managedDepartment;
    }

    public User(int userId, String username, String email, String firstName, String lastName, String birthday, UserRole role, Department department, Department managedDepartment) {
        this.userId = userId;
        this.username = username;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.role = role;
        this.department = department;
        this.managedDepartment = managedDepartment;
    }

    public User(int userId, String username, String email, String password, String firstName, String lastName, String birthday, UserRole role, Department department, Department managedDepartment) {
        this.userId = userId;
        this.username = username;
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.role = role;
        this.department = department;
        this.managedDepartment = managedDepartment;
    }
}
