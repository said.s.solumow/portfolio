package com.iperiaA.iperia.dtos;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MessageResponseDTO {

    private int messageId;
    private String messageContent;
    private String timestamp;
    private int senderId;
    private int receiverId;
}
