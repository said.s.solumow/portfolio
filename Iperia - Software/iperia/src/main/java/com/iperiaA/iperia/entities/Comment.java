package com.iperiaA.iperia.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "comments")
@Getter
@Setter
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int commentId;
    private String commentText;
    @ManyToOne
    @JoinColumn(name = "authorId")
    private User author;

    @OneToOne(mappedBy = "comment")
    private Ticket ticket;

    public Comment() {
    }

    public Comment(String commentText, User author) {
        this.commentText = commentText;
        this.author = author;
    }

    public Comment(int commentId, String commentText, User author, Ticket ticket) {
        this.commentId = commentId;
        this.commentText = commentText;
        this.author = author;
        this.ticket = ticket;
    }
}
