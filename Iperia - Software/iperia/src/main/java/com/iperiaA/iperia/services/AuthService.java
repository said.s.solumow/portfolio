package com.iperiaA.iperia.services;

import com.iperiaA.iperia.dtos.AuthRequestDTO;
import com.iperiaA.iperia.dtos.AuthResponseDTO;
import com.iperiaA.iperia.dtos.UserDataDTO;
import com.iperiaA.iperia.entities.User;
import com.iperiaA.iperia.entities.UserRole;
import com.iperiaA.iperia.repositories.UserRepository;
import com.iperiaA.iperia.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class AuthService {


    @Autowired
    UserRepository userRepository;

    private final AuthenticationManager authenticationManager;
    private final UserDetailsService userDetailsService;
    private final JwtUtil jwtTokenUtil;

    public AuthService(AuthenticationManager authenticationManager, UserDetailsService userDetailsService, JwtUtil jwtTokenUtil) {
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
        this.jwtTokenUtil = jwtTokenUtil;
    }

    public ResponseEntity<?> auth(AuthRequestDTO authRequestDTO) {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken
                = new UsernamePasswordAuthenticationToken(
                authRequestDTO.getUsername(),
                authRequestDTO.getPassword());

        authenticationManager.authenticate(usernamePasswordAuthenticationToken);

        Map<String, Object> claims = new HashMap<>();
        User user = userRepository.findByUsername(authRequestDTO.getUsername());
        claims.put("Role", user.getRole().getName());

        UserDetails userDetails = userDetailsService.loadUserByUsername(authRequestDTO.getUsername());
        String token = jwtTokenUtil.generateToken(claims, userDetails);

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(new AuthResponseDTO(token, user.getRole().getName(), user.getUsername(), user.getEmail()));
    }

    public ResponseEntity<?> checkIfLoggedIn() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        User user = userRepository.findByUsername(auth.getName());

        UserDataDTO userDataDTO;
        if (user.getRole().getName().equals("MANAGER")) {
            userDataDTO = new UserDataDTO(
                    user.getUserId(),
                    user.getUsername(),
                    user.getEmail(),
                    user.getFirstName(),
                    user.getLastName(),
                    user.getBirthday(),
                    user.getDepartment().getDepartmentName(),
                    user.getRole().getName(),
                    user.getManagedDepartment().getDepartmentName());
        }

        userDataDTO = new UserDataDTO(
                user.getUserId(),
                user.getUsername(),
                user.getEmail(),
                user.getFirstName(),
                user.getLastName(),
                user.getBirthday(),
                user.getDepartment().getDepartmentName(),
                user.getRole().getName(),
                null);

        return ResponseEntity.status(HttpStatus.OK).body(userDataDTO);
    }


}
