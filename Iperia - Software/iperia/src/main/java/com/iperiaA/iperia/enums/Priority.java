package com.iperiaA.iperia.enums;

public enum Priority {
    VERY_IMPORTANT,
    IMPORTANT,
    MODERATE,
    LESS_IMPORTANT
}
