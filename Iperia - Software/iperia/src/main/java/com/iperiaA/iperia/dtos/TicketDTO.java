package com.iperiaA.iperia.dtos;

import lombok.*;
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TicketDTO {
    private String title;
    private String description;
    private String priority;
    private String department;
    private String fileName;
}
