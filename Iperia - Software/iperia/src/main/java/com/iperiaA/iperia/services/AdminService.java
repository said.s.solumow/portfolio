package com.iperiaA.iperia.services;

import com.iperiaA.iperia.dtos.*;
import com.iperiaA.iperia.entities.Department;
import com.iperiaA.iperia.entities.User;
import com.iperiaA.iperia.entities.UserRole;
import com.iperiaA.iperia.exceptions.UserNotFoundException;
import com.iperiaA.iperia.repositories.DepartmentRepository;
import com.iperiaA.iperia.repositories.UserRepository;
import com.iperiaA.iperia.repositories.UserRoleRepository;
import com.iperiaA.iperia.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class AdminService {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserRoleRepository userRoleRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    JwtUtil jwtUtil;

    private final UserDetailsService userDetailsService;

    public AdminService(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }


    public ResponseEntity<?> register(RegisterDTO registerDTO) {
        User user = createUser(registerDTO);

        Map<String, Object> claims = new HashMap<>();

        claims.put("Role", user.getRole());

        UserDetails userDetails = userDetailsService.loadUserByUsername(registerDTO.getUsername());

        String jwt = jwtUtil.generateToken(claims, userDetails);

        return ResponseEntity.status(HttpStatus.CREATED).body(new UserDTO(user.getUserId(), user.getUsername(), user.getEmail(), user.getRole().getName(), jwt));
    }

    public ResponseEntity<?> updateUserData(int userId, UpdateUserDTO updateUserDTO) {
        if (updateUserDTO.getRole().equals("MANAGER")) {
            User updatedUser = constructUserIfManager(userId, updateUserDTO);
            userRepository.save(updatedUser);
            UserDataDTO userDataDTO = constructUserDataDTOIfManager(updatedUser);
            return ResponseEntity.status(HttpStatus.OK).body(userDataDTO);

        } else {
            User updatedUser = constructUser(userId, updateUserDTO);
            userRepository.save(updatedUser);
            UserDataDTO userDataDTO = constructUserDataDTO(updatedUser);
            return ResponseEntity.status(HttpStatus.OK).body(userDataDTO);
        }
    }

    public ResponseEntity<?> deleteUser(int userId) {
        userRepository.deleteById(userId);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    public User getUserWithId(int userId) {
        Optional<User> userOptional = userRepository.findById(userId);
        if (userOptional.isEmpty()) {
            throw new UserNotFoundException("User with ID: " + userId + " not found!");
        }
        return userOptional.get();
    }

    public User createUser(RegisterDTO registerDTO) {
        String hashedPassword;

        hashedPassword = passwordEncoder.encode(registerDTO.getPassword());

        UserRole userRole = userRoleRepository.findByName(registerDTO.getRole());

        Department department = departmentRepository.findByDepartmentName(registerDTO.getDepartment());
        Department managedDepartment = departmentRepository.findByDepartmentName(registerDTO.getManagedDepartment());

        User user;

        if (registerDTO.getRole().equals("MANAGER")) {
            user = new User(
                    registerDTO.getUsername(),
                    registerDTO.getEmail(),
                    hashedPassword,
                    registerDTO.getFirstName(),
                    registerDTO.getLastName(),
                    registerDTO.getBirthday(),
                    userRole,
                    department,
                    managedDepartment);
        } else {
            user = new User(
                    registerDTO.getUsername(),
                    registerDTO.getEmail(),
                    hashedPassword,
                    registerDTO.getFirstName(),
                    registerDTO.getLastName(),
                    registerDTO.getBirthday(),
                    userRole,
                    department);
        }

        userRepository.save(user);

        return user;
    }

    public ResponseEntity<?> createDepartment(DepartmentDTO departmentDTO) {
        Department department = new Department(departmentDTO.getDepartmentName());
        departmentRepository.save(department);

        DepartmentResponseDTO departmentResponseDTO = new DepartmentResponseDTO(
                department.getDepartmentId(),
                department.getDepartmentName());

        return ResponseEntity.status(HttpStatus.CREATED).body(departmentResponseDTO);
    }

    public int getDepartmentIdByDepartmentName(String departmentName) {
        Iterable<Department> departments = departmentRepository.findAll();

        int departmentId = 0;

        for (Department department : departments) {
            if (department.getDepartmentName().equals(departmentName)) {
                return departmentId = department.getDepartmentId();
            }
        }
        return 0;
    }

    public User constructUser(int userId, UpdateUserDTO updateUserDTO) {
        User user = getUserWithId(userId);

        UserRole updatedUserRole = userRoleRepository.findByName(updateUserDTO.getRole());
        Department updatedDepartment = departmentRepository.findByDepartmentName(updateUserDTO.getDepartment());

        return new User(
                user.getUserId(),
                user.getUsername(),
                updateUserDTO.getEmail(),
                user.getPassword(),
                updateUserDTO.getFirstName(),
                updateUserDTO.getLastName(),
                updateUserDTO.getBirthday(),
                updatedUserRole,
                updatedDepartment,
                user.getManagedDepartment());
    }

    public User constructUserIfManager(int userId, UpdateUserDTO updateUserDTO) {
        User user = getUserWithId(userId);

        UserRole updatedUserRole = userRoleRepository.findByName(updateUserDTO.getRole());
        Department updatedDepartment = departmentRepository.findByDepartmentName(updateUserDTO.getDepartment());

        Department managedDepartment = departmentRepository.findByDepartmentName(updateUserDTO.getManagedDepartment());

        return new User(
                user.getUserId(),
                user.getUsername(),
                updateUserDTO.getEmail(),
                user.getPassword(),
                updateUserDTO.getFirstName(),
                updateUserDTO.getLastName(),
                updateUserDTO.getBirthday(),
                updatedUserRole,
                updatedDepartment,
                managedDepartment);
    }

    public UserDataDTO constructUserDataDTO(User updatedUser) {
        return new UserDataDTO(
                updatedUser.getUserId(),
                updatedUser.getUsername(),
                updatedUser.getEmail(),
                updatedUser.getFirstName(),
                updatedUser.getLastName(),
                updatedUser.getBirthday(),
                updatedUser.getDepartment().getDepartmentName(),
                updatedUser.getRole().getName(),
                null);
    }

    public UserDataDTO constructUserDataDTOIfManager(User updatedUser) {
        return new UserDataDTO(
                updatedUser.getUserId(),
                updatedUser.getUsername(),
                updatedUser.getEmail(),
                updatedUser.getFirstName(),
                updatedUser.getLastName(),
                updatedUser.getBirthday(),
                updatedUser.getDepartment().getDepartmentName(),
                updatedUser.getRole().getName(),
                updatedUser.getManagedDepartment().getDepartmentName());
    }
}
