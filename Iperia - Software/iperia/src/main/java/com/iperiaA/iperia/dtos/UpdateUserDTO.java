package com.iperiaA.iperia.dtos;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class UpdateUserDTO {
    private String email;
    private String firstName;
    private String lastName;
    private String birthday;
    private String role;
    private String department;
    private String managedDepartment;

}
