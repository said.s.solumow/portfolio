package com.iperiaA.iperia.dtos;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FileResponseDTO {

    private int fileId;
    private int ticketId;
    private String fileName;
    private String url;
}
