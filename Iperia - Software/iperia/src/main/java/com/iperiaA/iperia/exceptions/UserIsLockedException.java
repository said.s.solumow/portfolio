package com.iperiaA.iperia.exceptions;

public class UserIsLockedException extends RuntimeException{

    public UserIsLockedException(String message) {
        super(message);
    }
}
