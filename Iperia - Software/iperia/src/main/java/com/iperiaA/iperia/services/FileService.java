package com.iperiaA.iperia.services;

import com.iperiaA.iperia.dtos.FileInfo;
import com.iperiaA.iperia.dtos.ResponseMessage;
import com.iperiaA.iperia.entities.FileMetaData;
import com.iperiaA.iperia.repositories.FileMetaDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileService {

    @Autowired
    FileUploadService storageService;
    @Autowired
    FileMetaDataRepository fileMetaDataRepository;


    public ResponseEntity<?> uploadFile(MultipartFile file) {
        String message = "";
        storageService.save(file);

        String fileName = file.getOriginalFilename();
        String filePath = System.getProperty("user.dir") + "/uploads/" + fileName;

        FileMetaData metaData = new FileMetaData(fileName, filePath);
        fileMetaDataRepository.save(metaData);

        FileInfo fileInfo = new FileInfo(fileName, filePath);

        message = "Uploaded the file successfully: " + fileName;
        return ResponseEntity.status(HttpStatus.OK).body(fileInfo);
    }
}
