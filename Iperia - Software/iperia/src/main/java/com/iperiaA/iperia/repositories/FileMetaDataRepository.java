package com.iperiaA.iperia.repositories;

import com.iperiaA.iperia.entities.FileMetaData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileMetaDataRepository extends JpaRepository<FileMetaData, Integer> {
    FileMetaData findByFileName(String fileName);

    FileMetaData findByTicket_TicketId(int ticketId);
}
