package com.iperiaA.iperia.services;


import com.iperiaA.iperia.dtos.DepartmentDTO;
import com.iperiaA.iperia.entities.Department;
import com.iperiaA.iperia.entities.User;
import com.iperiaA.iperia.repositories.DepartmentRepository;
import com.iperiaA.iperia.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class DepartmentService {


    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    UserRepository userRepository;

    public ResponseEntity<?> getDepartments() {
        List<String> departmentDTOList = new ArrayList<>();
        Iterable<Department> departments = departmentRepository.findAll();
        for (Department department : departments) {
            String departmentDTO = department.getDepartmentName();
            departmentDTOList.add(departmentDTO);
        }
        return ResponseEntity.status(HttpStatus.OK).body(departmentDTOList);
    }


    //DOES NOT WORK CORrECTLY
    public ResponseEntity<?> updateDepartment(String departmentName, DepartmentDTO departmentDTO) {
        Department department = departmentRepository.findByDepartmentName(departmentName);
        department.setDepartmentName(departmentDTO.getDepartmentName());
        departmentRepository.save(department);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    public ResponseEntity<?> deleteDepartment(String departmentName) {
        Iterable<User> userIterable = userRepository.findAll();
        for (User user : userIterable) {
            if (Objects.equals(user.getDepartment().getDepartmentName(), departmentName)) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body(
                        "Die Abteilung darf nicht gelöscht werden, " +
                                "weil noch Mitarbeiter in der Abteilung arbeiten.");
            }
        }
        Department department = departmentRepository.findByDepartmentName(departmentName);
        departmentRepository.delete(department);
        return ResponseEntity.status(HttpStatus.OK).build();
    }





}
