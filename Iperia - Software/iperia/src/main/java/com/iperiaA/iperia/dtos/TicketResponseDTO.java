package com.iperiaA.iperia.dtos;

import com.iperiaA.iperia.entities.Comment;
import com.iperiaA.iperia.entities.Department;
import com.iperiaA.iperia.entities.FileMetaData;
import com.iperiaA.iperia.entities.User;
import com.iperiaA.iperia.enums.Priority;
import com.iperiaA.iperia.enums.TicketStatus;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TicketResponseDTO {

    private int ticketId;
    private String title;
    private String description;
    private String createdAt;
    private String resolvedAt;
    private String priority;
    private String ticketStatus;
    private String createdBy;
    private String resolvedBy;
    private String department;
}
