package com.iperiaA.iperia.controller;

import com.iperiaA.iperia.dtos.MessageRequestDTO;
import com.iperiaA.iperia.services.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/messages")
public class MessageController {

    @Autowired
    MessageService messageService;


    @PostMapping("{receiverId}")
    public ResponseEntity<?> postMessage(@PathVariable int receiverId, @RequestBody MessageRequestDTO messageRequestDTO) {
        try {
            return messageService.postMessage(receiverId, messageRequestDTO);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @GetMapping("{receiverId}/{senderId}")
    public ResponseEntity<?> getMessages(@PathVariable int receiverId, @PathVariable int senderId) {
        try {
            return messageService.getMessages(receiverId, senderId);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }
}
