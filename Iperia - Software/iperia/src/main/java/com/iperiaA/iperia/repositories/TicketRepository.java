package com.iperiaA.iperia.repositories;

import com.iperiaA.iperia.entities.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Integer> {

    Ticket findByTitle(String title);
}
