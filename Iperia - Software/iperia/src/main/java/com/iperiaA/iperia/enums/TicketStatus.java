package com.iperiaA.iperia.enums;

public enum TicketStatus {
    OPEN,
    PENDING,
    RESOLVED,
    PROBLEM
}
