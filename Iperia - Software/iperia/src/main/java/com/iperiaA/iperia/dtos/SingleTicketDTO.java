package com.iperiaA.iperia.dtos;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SingleTicketDTO {

    private int ticketId;
    private String title;
    private String description;
    private String createdAt;
    private String resolvedAt;
    private String priority;
    private String ticketStatus;
    private String fileName;
    private String createdBy;
    private String resolvedBy;
    private String department;
    private String comment;
}
