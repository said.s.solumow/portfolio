package com.iperiaA.iperia.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "messages")
@Getter
@Setter
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int messageId;
    private String messageContent;
    private String timestamp;
    @ManyToOne
    @JoinColumn(name = "senderId")
    private User sender;
    @ManyToOne
    @JoinColumn(name = "receiverId")
    private User receiver;

    public Message() {
    }

    public Message(String messageContent, String timestamp, User sender, User receiver) {
        this.messageContent = messageContent;
        this.timestamp = timestamp;
        this.sender = sender;
        this.receiver = receiver;
    }

    public Message(int messageId, String messageContent, String timestamp, User sender, User receiver) {
        this.messageId = messageId;
        this.messageContent = messageContent;
        this.timestamp = timestamp;
        this.sender = sender;
        this.receiver = receiver;
    }
}
