package com.iperiaA.iperia.dtos;

import lombok.*;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MessageRequestDTO {
    private String messageContent;
    private int senderId;
}
