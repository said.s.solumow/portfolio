package com.iperiaA.iperia.dtos;

import com.iperiaA.iperia.entities.UserRole;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AuthResponseDTO {
    private String jwt;
    private String role;
    private String user;
    private String email;
}
