package com.iperiaA.iperia.entities;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "file_meta_data")
@Getter
@Setter
public class FileMetaData {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int fileId;
    private String fileName;
    private String fileUrl;
    @ManyToOne
    @JoinColumn(name = "ticketId")
    private Ticket ticket;

    public FileMetaData() {
    }

    public FileMetaData(String fileName, String fileUrl) {
        this.fileName = fileName;
        this.fileUrl = fileUrl;
    }

    public FileMetaData(int fileId, String fileName, String fileUrl, Ticket ticket) {
        this.fileId = fileId;
        this.fileName = fileName;
        this.fileUrl = fileUrl;
        this.ticket = ticket;
    }
}
