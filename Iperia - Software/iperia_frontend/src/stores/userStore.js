import {defineStore} from "pinia";
import {ref} from "vue";
import axios from "axios";
import {router} from "@/router";

export const useUserStore = defineStore("user", () => {

    const user = ref({});
    const users = ref([])

    function getUserData(userData) {
        user.value = userData;
    }

    async function registerUser(user) {
        let response = await axios.post(
            "http://localhost:8080/api/admin/register", user, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            });
    }

    async function getUsers(page = 0) {
        let response = await axios.get(
            `http://localhost:8080/api/user?page=${page}`, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            });
        users.value = response.data;
    }

    async function getAllUsers() {
        let response = await axios.get(
            "http://localhost:8080/api/user/all", {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            });
        console.log(response);
        users.value = response.data;
    }

    async function getSingleUser(userId) {
        let response = await axios.get(
            "http://localhost:8080/api/user/" + userId, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            });
        user.value = response.data;
    }

    async function editUser(userId, newUser) {
        let response = await axios.put(
            "http://localhost:8080/api/admin/" + userId, newUser, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            });
    }

    async function deleteUser(userId) {
        let response = await axios.delete(
            "http://localhost:8080/api/admin/" + userId, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            });
    }

    async function changePassword(userId, userPassword) {
        let response = await axios.put(
            "http://localhost:8080/api/user/" + userId, userPassword, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            });
    }

    return {
        user,
        users,
        registerUser,
        getUsers,
        deleteUser,
        editUser,
        getUserData,
        changePassword,
        getSingleUser,
        getAllUsers
    }
});
