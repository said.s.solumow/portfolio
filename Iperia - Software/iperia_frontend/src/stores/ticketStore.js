import axios from 'axios';
import { defineStore } from 'pinia';
import { ref } from 'vue';

export const useTicketStore = defineStore('ticket', () => {

    const currentTicket = ref({})
    const resolvedBy = ref("")
    const ticket = ref({})
    const tickets = ref([])
    const comment = ref("")
    const ticketsForChart = ref([])


    async function createTicket(username, ticket) {
        let response = await axios.post(
            "http://localhost:8080/api/ticket/" + username, ticket, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            });
    }

    async function putTicketFileRelation(fileName, ticket) {
        let response = await axios.put(
            "http://localhost:8080/api/ticket/" + fileName, ticket, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            });
    }

    async function removeTicketFileRelation(fileName) {
        let response = await axios.delete(
            "http://localhost:8080/api/ticket/remove/" + fileName, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            });
    }

    async function getTickets(page = 0) {
        let response = await axios.get(
            `http://localhost:8080/api/ticket?page=${page}`,  {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            });
        tickets.value = response.data;
    }

    async function getAllTickets() {
        let response = await axios.get(
            "http://localhost:8080/api/ticket/all", {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            });
        ticketsForChart.value = response.data;
    }

    async function getTicket(ticketId) {
        let response = await axios.get(
            "http://localhost:8080/api/ticket/" + ticketId, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            });
        resolvedBy.value = response.data.resolvedBy
        ticket.value = response.data
    }

    async function deleteTicket(ticketId) {
        let response = await axios.delete(
            "http://localhost:8080/api/ticket/" + ticketId, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            });
    }

    async function acceptTicket(ticketId, ticket) {
        let response = await axios.put(
            "http://localhost:8080/api/ticket/status/" + ticketId, ticket, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            });
    }

    async function returnAcceptedTicket(ticketId, ticket) {
        let response = await axios.put(
            "http://localhost:8080/api/ticket/status/return/" + ticketId, ticket,{
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            });
    }

    async function finishTicket(ticketId, ticket) {
        let response = await axios.put(
            "http://localhost:8080/api/ticket/status/finish/" + ticketId, ticket, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            });
    }

    async function problemTicket(ticketId, ticket) {
        let response = await axios.put(
            "http://localhost:8080/api/ticket/status/problem/" + ticketId, ticket, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            });
        console.log(response);
    }

    async function addComment(ticketId, comment) {
        let response = await axios.post(
            "http://localhost:8080/api/ticket/" + ticketId + "/comment", comment, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            });
        comment.value = response.data.commentText;
    }

    return {
        ticket,
        tickets,
        ticketsForChart,
        resolvedBy,
        currentTicket,
        comment,
        createTicket,
        putTicketFileRelation,
        getTickets,
        getAllTickets,
        getTicket,
        deleteTicket,
        acceptTicket,
        returnAcceptedTicket,
        finishTicket,
        addComment,
        problemTicket,
        removeTicketFileRelation
    };
});