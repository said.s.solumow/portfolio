import {defineStore} from "pinia";
import {ref} from "vue";
import axios from "axios";
import {router} from "@/router";

export const useMessagesStore = defineStore("message", () => {

    const messagesSent = ref([])
    const messagesReceived = ref([])

    async function postMessage(receiverId, message) {
        let response = await axios.post(
            "http://localhost:8080/api/messages/" + receiverId, message, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            });
        console.log(response);
    }

    //SENDER ID ALWAYS SECOND VARIABLE (true for both functions)
    async function getMessagesSent(receiverId, senderId) {
        let response = await axios.get(
            "http://localhost:8080/api/messages/" + receiverId + "/" + senderId, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            });
        console.log(response);
        messagesSent.value = response.data;
    }

    async function getMessagesReceived(senderId, receiverId) {
        let response = await axios.get(
            "http://localhost:8080/api/messages/" + senderId + "/" + receiverId, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            });
        console.log(response);
        messagesReceived.value = response.data;
    }




    return {
        messagesSent,
        messagesReceived,
        postMessage,
        getMessagesSent,
        getMessagesReceived
    }
});
