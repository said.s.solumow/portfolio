//TODO: Zeitbuchung
// 	stunden und minuten im ticket
// Mitarbeiterprofil
// 	einzelne mitarbeiter profile anzeigen

import {defineStore} from "pinia";
import {ref} from "vue";
import axios from "axios";
import {router} from "@/router";

export const useAuthStore = defineStore("auth", () => {

    const auth = ref({});
    let userRole = ("");
    const user = ref({})

    async function login(credentials) {
        const response = await axios.post(
            "http://localhost:8080/api/auth", credentials);
        userRole = response.data.role;
        localStorage.setItem("token", response.data.jwt);
    }

    async function checkIfLoggedIn() {
        let response = await axios.get("http://localhost:8080/api/auth", {
            headers: {
                "Authorization": localStorage.getItem("token")
            }
        });
        user.value = response.data
    }

    async function logUserOut() {
        localStorage.removeItem("token")
        await router.push("/")
    }

    return {
        auth,
        user,
        userRole,
        checkIfLoggedIn,
        login,
        logUserOut}
});
