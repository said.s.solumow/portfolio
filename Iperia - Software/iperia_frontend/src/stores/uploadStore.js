import axios from 'axios';
import { defineStore } from 'pinia';
import { ref } from 'vue';

export const useUploadStore = defineStore('upload', () => {

    const files = ref([])
    const fileName = ref("");
    const file = ref({});

    async function getFiles() {
        let response = await axios.get(
            "http://localhost:8080/api/files", {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            });
        files.value = response.data;
    }

    async function getSingleFile(fileName) {
        let response = await axios.get(
            "http://localhost:8080/api/files/" + fileName, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            });
        file.value = response.data;
    }

    return {
        files,
        file,
        getFiles,
        getSingleFile
    };
});