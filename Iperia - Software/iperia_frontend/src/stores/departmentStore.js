import {defineStore} from "pinia";
import {ref} from "vue";
import axios from "axios";
import {router} from "@/router";

export const useDepartmentStore = defineStore("department", () => {

    const departments = ref([])


    async function getDepartments() {
        let response = await axios.get(
            "http://localhost:8080/api/department", {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            });
        departments.value = response.data;
    }


    async function createDepartment(dep) {
        let response = await axios.post(
            "http://localhost:8080/api/admin/department", dep, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            });
    }

    async function updateDepartment(departmentName, dep) {
        let response = await axios.put(
            "http://localhost:8080/api/department/update/" + departmentName, dep, {
            headers: {
                Authorization: localStorage.getItem("token")
            }
        });
        await getDepartments();
    }

    async function deleteDepartment(departmentName) {
        let response = await axios.delete(
            "http://localhost:8080/api/department/" + departmentName, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            });
    }



    return {
        departments,
        createDepartment,
        getDepartments,
        deleteDepartment,
        updateDepartment
    }
});
