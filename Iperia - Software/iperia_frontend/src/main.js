import { createApp } from 'vue'
import { createPinia } from 'pinia'
import 'bootstrap/dist/css/bootstrap.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

import App from './App.vue'
import {router} from "@/router";
import {vuetify} from "@/plugins/vuetify"

const pinia = createPinia();

createApp(App)
    .use(pinia)
    .use(router)
    .use(vuetify)
    .mount('#app')
