import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import GeneralLayout from "@/layouts/GeneralLayout.vue";
import AuthenticationLayout from "@/layouts/AuthenticationLayout.vue";
import RegistrationView from "@/views/RegistrationView.vue";
import LoginView from "@/views/LoginView.vue";
import UserProfile from "@/views/ProfileView.vue";
import EmployeesDisplay from "@/views/EmployeesView.vue";
import TicketsView from "@/views/TicketsView.vue";
import DepartmentView from "@/views/DepartmentView.vue";
import TicketsHistoryView from "@/views/TicketsHistoryView.vue";
import SingleTicketView from "@/views/SingleTicketView.vue";
import HelpView from "@/views/HelpView.vue";
import SingleEmployeeView from "@/views/SingleEmployeeView.vue";
import MessagesView from "@/views/MessagesView.vue";

const routes = [
  {path: '/dashboard', component: GeneralLayout,
    children: [
      {
        path: '',
        component: HomeView
      },
      {
        path: '/messages',
        component: MessagesView
      },
      {
        path: '/profile',
        component: UserProfile
      },
      {
        path: '/employees',
        component: EmployeesDisplay
      },
      {
        path: '/tickets',
        component: TicketsView
      },
      {
        path: '/history',
        component: TicketsHistoryView
      },
      {
        path:'/ticket/:id', name: 'SingleTicketView', component: SingleTicketView
      },
      {
        path: '/help',
        component: HelpView
      },
      {
        path: '/employee/:id', name: 'SingleEmployeeView', component: SingleEmployeeView
      }
    ]},
  {
    path: '/',
    component: AuthenticationLayout,
    children: [
      {
        path: '',
        component: LoginView
      },
      {
        path: '/register',
        component: RegistrationView
      },
      {
        path: '/department',
        component: DepartmentView
      }
    ]
  }
  /*{ path: "/product/:id", name: "SingleProductView", component: SingleProductView }*/
]

const router = createRouter({
  routes,
  history: createWebHistory()
})



export {router}
